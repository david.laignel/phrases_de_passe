from bloc1_phrases_de_passe_liste_7776_mots import LISTE_MOTS

def verification_a_faire(liste):
    '''
    : liste : (tuple) liste à vérifier
    elle doit être de type tuple, comporter 7776 chaînes de caractères
    ne contenant que des lettres (non accentuées ) et toutes différentes
    CU : aucune
    : effets de bord : aucun
    : return : (booleen) True si les conditions sont respectées, False sinon
    
    >>> verification_a_faire(LISTE_MOTS)
    True
    
    '''
    bool = True
    if type(liste)!=tuple : # on vérifie le type tuple
       return False
    if len(liste)!=7776 : # on vérifie la longueur
        return False
   # vérification sur les mots de la liste
        if type(all(liste))!=str : # on vérifie qu'il s'agit d'une chaîne de caractères
            return False
        # on vérifie que les mots ne sont présents qu'une seule fois
        l=conversion_liste_d_un_tupe(liste)
        l.remove(mot)
        if mot in l :
          return False
        # on vérifie que les mots ne contiennent pas de caractères accentués
        for caractere in mot :
            if not(caractere in 'abcdefghijklmnopqrstuvwxyz') :
                 return False
    return True

def conversion_liste_d_un_tupe(variable_tuple) :
    '''
    convertit un tuple en liste
    : parametre  variable_tuple : (tuple ) un tuple
    : return : (list) une liste dont les éléments sont identiques à ceux du tuple
    : effets de bords : aucun
    : CU :
    
    >>>conversion_liste_d_un_tupe((0,1,2))
    [0, 1, 2]
    
    
    '''
    l=[]
    for i in variable_tuple :
        l.append(i)
    return l
def longueur_minimale_des_mots(montuple) :
    '''
    retourne la longueur du plus petit mot de la liste de mots
    : montuple : (tuple ) une liste fixée de mots
    : return : (int) la longueur du plus petit mot
    
    >>>longueur_minimale_des_mots(LISTE_MOTS)
    4
    
    '''
    longueur_minimale=len(montuple[0])
    for mot in montuple :
        if len(mot)<longueur_minimale :
            longueur_minimale=len(mot)
    return longueur_minimale

def longueur_maximale_des_mots(montuple) :
    '''
    retourne la longueur du plus petit mot de la liste de mots
    : montuple : (tuple ) une liste fixée de mots
    : return : (int) la longueur du plus petit mot
    '''
    longueur_max=len(montuple[0])
    for mot in montuple :
        if len(mot)>longueur_max :
            longueur_max=len(mot)
    return longueur_max
